import React, { useEffect, useRef, useState } from "react";
import { Hero, Skeletons, Sticked } from "./components/ui";

import "./App.scss";
import { Filters, FundsList, Search, TopBar } from "./components";
import { FundsServices } from "./services";

function App() {
  const [data, setData] = useState();
  const [dataSource, setDataSource] = useState();
  const [minimalRange, setMinimalRange] = useState();
  const [rescueRange, setRescueRange] = useState();
  const [riskRange, setRiskRange] = useState();
  const [initialized, setInitialized] = useState(false);
  const [filters, setFilters] = useState([]);
  const [search, setSearch] = useState({});

  const appRef = useRef();

  const getFunds = () => {
    FundsServices.getFunds().then((response) => {
      setMinimalRange(response.minimalRange);
      setRescueRange(response.rescueRange);
      setRiskRange(response.riskRange);
      setDataSource(response.data);
    });
  };

  useEffect(() => {
    if (!initialized) {
      getFunds();
      setInitialized(true);
    }
  }, [initialized, appRef]);

  useEffect(() => {
    setData(FundsServices.filter(dataSource, filters, search));
  }, [dataSource, filters, search]);

  return (
    <main className="App" ref={appRef}>
      <header className="App-header">
        <div className="grid-container">
          <TopBar />
        </div>
        <Hero>
          <h1>Fundos de Investimento</h1>
          <p>Conheça a nossa lista de fundos</p>
        </Hero>
      </header>

      <article className="grid-container">
        <div className="grid-x grid-margin-x" id="fundsList">
          <div className="cell small-12 large-9">
            <Search
              onChange={setSearch}
              minimalRange={minimalRange}
              rescueRange={rescueRange}
              riskRange={riskRange}
            />
          </div>
          <div className="cell small-12 large-3" data-sticky-container>
            <Sticked id="fundsFilters" anchor="fundsList">
              <div className="card">
                {dataSource?.length ? (
                  <Filters dataSource={dataSource} onChange={setFilters} />
                ) : (
                  <Skeletons type="table" options={{ rows: 3 }} />
                )}
              </div>
            </Sticked>
          </div>
          <div className="cell small-12 large-9">
            <FundsList dataSource={data} />
          </div>
        </div>
        <div className="grid-x grid-margin-x">
          <div className="cell shrink">
            <div className="card">
              <div className="card-section">
                <dl className="text-nowrap subtitles">
                  <dt className="title">Legendas</dt>
                  <dd className="item">
                    <span className="mdi mdi-star-circle text-success icon"></span>
                    Fundo para investidor qualificado
                  </dd>
                  <dd className="item">
                    <span className="mdi mdi-check-circle text-medium-gray icon"></span>
                    Você já investe neste fundo
                  </dd>
                  <dd className="item">
                    <span className="mdi mdi-alert-circle-outline text-dark-gray icon"></span>
                    Entenda o resgate deste fundo
                  </dd>
                  <dd className="item">
                    <span className="mdi mdi-block-helper text-medium-gray icon"></span>
                    Fundo fechado para aplicação
                  </dd>
                  <dd className="item">
                    <span className="mdi mdi-reply-circle mdi-rotate-90 text-primary icon"></span>
                    Aplicar neste fundo
                  </dd>
                </dl>
              </div>
            </div>
          </div>
        </div>
      </article>
    </main>
  );
}

export default App;
