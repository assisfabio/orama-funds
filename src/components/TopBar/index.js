import React, { useEffect, useState } from "react";
import "./style.scss";
import logo from "assets/img/logo.svg";

const TopBar = () => {
  const [menu, setMenu] = useState(false);

  useEffect(() => {
    const root = document.querySelector("html");
    if (menu) {
      root.classList.add("nooverflow");
    } else {
      root.classList.remove("nooverflow");
    }
  }, [menu]);

  return (
    <div className="top-bar">
      <div
        className={`menu-icon ${menu ? "opened" : ""}`}
        onClick={() => setMenu(!menu)}
      ></div>
      <div className="top-bar-left">
        <div className="brand">
          <img src={logo} alt="Órama" />
        </div>
      </div>
      <div className={`top-bar-left menu-container ${menu ? "show" : ""}`}>
        <ul className="menu">
          <li>
            <a href="https://orama.com.br/empresa">A Empresa</a>
          </li>
          <li>
            <a href="https://orama.com.br/como-funciona">Como Funciona</a>
          </li>
          <li>
            <a href="https://orama.com.br/investimentos">Investimentos</a>
          </li>
          <li>
            <a href="https://orama.com.br/atendimento">Atendimento</a>
          </li>
        </ul>
      </div>
      <div className="top-bar-right account">
        <a
          className="btn btn-success outline"
          href="https://minhaconta.orama.com.br/login"
        >
          Sua Conta
        </a>
        <a
          className="btn btn-success"
          href="https://minhaconta.orama.com.br/novo-cadastro"
        >
          Abra Sua Conta
        </a>
      </div>
    </div>
  );
};

export default TopBar;
