import React, { useEffect, useState } from "react";
import {
  CardMacro,
  CardMain,
  FundRow,
  FundHeader,
  Skeletons,
} from "components/ui";

const FundsList = ({ dataSource }) => {
  const [data, setData] = useState(dataSource);

  useEffect(() => {
    if (dataSource) {
      setData(dataSource);
    }
  }, [dataSource]);

  return (
    <div className="card">
      <FundHeader className="show-for-medium" />
      {data?.length ? (
        data.map((macro, i) => (
          <CardMacro key={i} title={macro.name} description={macro.description}>
            {macro.mains.map((main, ii) => (
              <CardMain
                key={ii}
                title={main.name}
                description={main.explanation}
              >
                {main.funds.map((item, iii) => (
                  <FundRow key={iii} fund={item} />
                ))}
              </CardMain>
            ))}
          </CardMacro>
        ))
      ) : (
        <Skeletons type="table" />
      )}
    </div>
  );
};

export default FundsList;
