import React, { useEffect, useRef, useState } from "react";
import * as Foundation from "foundation-sites";
import $ from "jquery";
import "./style.scss";

const Tooltip = ({ children, className, position, text, theme }) => {
  const elemRef = useRef();
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (elemRef?.current && !initialized) {
      setInitialized(true);
    }
  }, [elemRef, initialized]);

  useEffect(() => {
    const elem = elemRef.current;
    if (initialized) {
      new Foundation.Tooltip($(elem));
    }
    // eslint-disable-next-line
  }, [initialized]);

  return (
    <div
      ref={elemRef}
      data-tooltip
      tabIndex="1"
      title={text}
      data-position={position}
      data-template-classes={theme === "light" ? "light" : ""}
      data-allow-html
      className={`tooltip-anchor ${className || ""}`}
    >
      {children}
    </div>
  );
};

export default Tooltip;
