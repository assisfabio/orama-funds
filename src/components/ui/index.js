import BgImage from "./BgImage";
import CardMacro from "./CardMacro";
import CardMain from "./CardMain";
import FundHeader from "./FundHeader";
import FundRow from "./FundRow";
import Hero from "./Hero/";
import InputRisk from "./InputRisk";
import InputSearch from "./InputSearch";
import InputSlider from "./InputSlider";
import Skeletons from "./Skeletons";
import Sticked from "./Sticked";
import Tooltip from "./Tooltip";

export {
  BgImage,
  CardMacro,
  CardMain,
  FundHeader,
  FundRow,
  Hero,
  InputRisk,
  InputSearch,
  InputSlider,
  Skeletons,
  Sticked,
  Tooltip,
};
