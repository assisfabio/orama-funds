import React from "react";
import Tooltip from "components/ui/Tooltip";
import "./style.scss";

const CardMain = ({ title, description, children, className }) => {
  return (
    <div className={`card-main ${className || ""}`}>
      <h5 className="card-main-title">
        {title}
        <Tooltip theme="light" text={description}>
          <span className="mdi mdi-help-circle"></span>
        </Tooltip>
      </h5>
      <div className="card-main-container">{children}</div>
    </div>
  );
};

export default CardMain;
