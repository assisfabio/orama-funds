import React, { useEffect, useRef, useState } from "react";
import * as Foundation from "foundation-sites";
import $ from "jquery";

import "./style.scss";

const InputSlider = ({ id, start, end, initial, onChange, onChanged }) => {
  const elemRef = useRef();
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (elemRef?.current && !initialized) {
      setInitialized(true);
    }
  }, [elemRef, initialized]);

  useEffect(() => {
    const elem = elemRef.current;
    if (initialized) {
      new Foundation.Slider($(elem));
      $(elem).on("changed.zf.slider", () => {
        onChanged($(`#${id}_output`)?.val());
      });
    }
    // eslint-disable-next-line
  }, [initialized]);

  return (
    <>
      <div
        ref={elemRef}
        className="slider slider-default"
        data-slider
        data-initial-start={initial === 0 ? initial : initial || start}
        data-end={end}
        data-start={start}
        data-step="1"
        data-changed-delay="100"
      >
        <span
          className="slider-handle"
          data-slider-handle
          role="slider"
          tabIndex="1"
          aria-valuemax=""
          aria-valuemin=""
          aria-valuenow=""
          aria-controls={`${id}_output`}
        ></span>
        <span className="slider-fill" data-slider-fill></span>
      </div>
      <input type="hidden" id={`${id}_output`} />
    </>
  );
};

export default InputSlider;
