import React from "react";
import Tooltip from "components/ui/Tooltip";
import "./style.scss";

const CardMacro = ({ title, description, children, className }) => {
  return (
    <div className={`card-macro ${className || ""}`}>
      <h4 className="card-macro-title">
        {title}
        <Tooltip theme="light" text={description}>
          <span className="mdi mdi-help-circle"></span>
        </Tooltip>
      </h4>
      <div className="card-macro-content">{children}</div>
    </div>
  );
};

export default CardMacro;
