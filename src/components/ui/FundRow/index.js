import React, { useEffect, useRef, useState } from "react";
import * as Foundation from "foundation-sites";
import $ from "jquery";

import Tooltip from "components/ui/Tooltip";
import Numbers from "utils/numbers";
import Dates from "utils/dates";

import "./style.scss";

const FundRow = ({ fund, className }) => {
  const elemRef = useRef();
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (elemRef?.current && !initialized && fund) {
      setInitialized(true);
    }
  }, [elemRef, initialized, fund]);

  useEffect(() => {
    const elem = elemRef.current;
    if (initialized && fund) {
      new Foundation.Accordion($(elem));
    }
  }, [initialized, fund]);

  return (
    <div
      className="accordion fund-row"
      data-accordion
      data-allow-all-closed="true"
      ref={elemRef}
      id={fund.id}
    >
      <div className="accordion-item" data-accordion-item>
        <a
          href={`https://www.orama.com.br/investimentos/fundos-de-investimento/${fund.slug}`}
          className="accordion-title"
        >
          <div
            className={`grid-x grid-padding-x grid-padding-y align-middle row-items risk_${
              fund.specification.fund_risk_profile.score_range_order
            } ${fund.fund_situation?.code === "3" ? "disabled" : ""} ${
              className || ""
            }`}
          >
            <div className="cell small-12 medium-4">
              <label className="hide-for-medium">Fundo</label>
              <h6 className="fund-name">
                {fund.simple_name}
                {fund.specification.is_qualified && (
                  <Tooltip text="Fundo para investidor qualificado">
                    <span className="mdi mdi-star-circle text-success"></span>
                  </Tooltip>
                )}
              </h6>
              <p className="fund-spec text-medium-gray">
                {`${fund.specification?.fund_type} | ${fund.specification?.fund_class}`}
              </p>
            </div>
            <div className="cell small-3 medium-auto">
              <label className="hide-for-medium">Data da cota</label>
              {Dates.default(fund.initial_date)}
            </div>
            <div className="cell small-3 medium-1">
              <label className="hide-for-medium">Mês(%)</label>
              {Numbers.format.float(fund.profitabilities.month)}
            </div>
            <div className="cell small-3 medium-1">
              <label className="hide-for-medium">
                {Dates.fullYear(new Date())}(%)
              </label>
              {Numbers.format.float(fund.profitabilities.year)}
            </div>
            <div className="cell small-3 medium-1">
              <label className="hide-for-medium">12M(%)</label>
              {Numbers.format.float(fund.profitabilities.m12)}
            </div>
            <div className="cell small-5 medium-auto">
              <label className="hide-for-medium">Aplicação mínima(R$)</label>
              {Numbers.format.float(
                fund.operability.minimum_initial_application_amount
              )}
            </div>
            <div className="cell small-5 medium-1">
              <label className="hide-for-medium">Prazo de resgate</label>
              {`D+${fund.operability.retrieval_liquidation_days}`}
            </div>
            <div className="cell medium-1 shrink col-action">
              {fund.fund_situation?.code === "3" ? (
                <span className="mdi mdi-block-helper text-medium-gray"></span>
              ) : (
                <span
                  role="link"
                  onClick={(e) => {
                    console.log("Abrir Modal");
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                  }}
                  className="mdi mdi-reply-circle mdi-rotate-90 text-primary text-hover"
                ></span>
              )}
            </div>
          </div>
        </a>
        <div className="accordion-content" data-tab-content>
          <div className="grid-x grid-padding-x grid-padding-y align-middle">
            <div className="cell small-12 medium-6">Gráfico</div>
            <div className="cell small-12 medium-6">
              <strong>Cotização da aplicação:</strong>{" "}
              {fund.operability.application_quotation_days_str}
              <Tooltip
                theme="light"
                text="Total de dias para que o valor aplicado seja convertido em cotas do fundo."
              >
                <span className="mdi mdi-help-circle text-dark-gray"></span>
              </Tooltip>
              <br />
              <strong>Cotização do resgate:</strong>{" "}
              {fund.operability.retrieval_quotation_days_str}
              {fund.operability.retrieval_quotation_days_str.indexOf("(") < 0
                ? ` (dias ${fund.operability.retrieval_quotation_days_type})`
                : ""}
              <Tooltip
                theme="light"
                text="Total de dias para que as cotas do fundo sejam transformadas em valor monetário."
              >
                <span className="mdi mdi-help-circle text-dark-gray"></span>
              </Tooltip>
              <br />
              <strong>Liquidação do resgate:</strong>{" "}
              {fund.operability.retrieval_liquidation_days_str}
              {fund.operability.retrieval_liquidation_days_str.indexOf("(") < 0
                ? ` (dias ${fund.operability.retrieval_liquidation_days_type})`
                : ""}
              <Tooltip
                theme="light"
                text="Total de dias após a conversão para que o valor do resgate esteja disponível em sua Subconta Órama."
              >
                <span className="mdi mdi-help-circle text-dark-gray"></span>
              </Tooltip>
              <br />
              <strong>Taxa de administração:</strong>{" "}
              {fund.fees.administration_fee}
              <Tooltip
                theme="light"
                text="Taxa anual cobrada pelo Administrador do Fundo como remuneração pelos serviços prestados."
              >
                <span className="mdi mdi-help-circle text-dark-gray"></span>
              </Tooltip>
              <br />
              <br />
              <a
                href={`https://www.orama.com.br/investimentos/fundos-de-investimento/${fund.slug}`}
              >
                Conheça mais informações sobre este fundo
              </a>
            </div>

            <div className="cell small-12 medium-6 medium-offset-6">
              <strong>CNPJ do fundo:</strong> {fund.cnpj}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default FundRow;
