import React from "react";
import "./style.scss";

const templates = {
  line: (options) => <div className="skeleton inline" {...options}></div>,
  table: (options) => (
    <>
      {[...Array(options?.rows || 10)].map((x, i) => (
        <div className="skeleton row" key={i}></div>
      ))}
    </>
  ),
};

const Skeletons = ({ type, height, width, options }) => {
  return (
    <div
      style={{ height: height, width: width }}
      className="skeleton-container"
    >
      {templates[type](options)}
    </div>
  );
};

export default Skeletons;
