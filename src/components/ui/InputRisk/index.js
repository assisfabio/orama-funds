import React, { useEffect, useRef, useState } from "react";
import * as Foundation from "foundation-sites";
import $ from "jquery";

import "./style.scss";

const Input = ({ id, start, end, initial, onChanged, riskRange }) => {
  const elemRef = useRef();
  const [initialized, setInitialized] = useState(false);

  const getRisk = (value) => {
    return riskRange && riskRange[value];
  };

  useEffect(() => {
    if (elemRef?.current && !initialized) {
      setInitialized(true);
    }
  }, [elemRef, initialized]);

  useEffect(() => {
    const elem = elemRef.current;
    if (initialized) {
      new Foundation.Slider($(elem));
      $(elem).on("changed.zf.slider", () => {
        onChanged(getRisk($(`#${id}_output`)?.val()));
      });
    }
    // eslint-disable-next-line
  }, [initialized]);

  return (
    <>
      <div className="grid-x align-middle risk-container">
        <div className="cell shrink">Menor</div>
        <div className="cell auto">
          <div
            ref={elemRef}
            className="slider slider-risk"
            data-slider
            data-initial-start={riskRange.length - 1}
            data-end={riskRange.length - 1}
            data-start={0}
            data-step="1"
            data-changed-delay="100"
          >
            <div className="risk-score">
              {[...Array(riskRange.length)].map((item, i) => (
                <span
                  key={i}
                  className={`score_${getRisk(i)}`}
                  style={{ height: `${(100 / riskRange.length) * (i + 1)}%` }}
                ></span>
              ))}
            </div>
            <span
              className="slider-handle"
              data-slider-handle
              role="slider"
              tabIndex="1"
              aria-valuemax=""
              aria-valuemin=""
              aria-valuenow=""
              aria-controls={`${id}_output`}
              style={{
                width: `${100 / riskRange.length}%`,
              }}
            ></span>
            <span className="slider-fill" data-slider-fill></span>
          </div>
          <input type="hidden" id={`${id}_output`} />
        </div>
        <div className="cell shrink">Maior</div>
      </div>
    </>
  );
};

export default Input;
