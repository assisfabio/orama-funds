import React, { useEffect, useRef, useState } from "react";
import * as Foundation from "foundation-sites";
import $ from "jquery";

const Sticked = ({ id, anchor, children, className }) => {
  const elemRef = useRef();
  const [initialized, setInitialized] = useState(false);

  useEffect(() => {
    if (elemRef?.current && !initialized) {
      new Foundation.Sticky($(elemRef.current));
      setInitialized(true);
    }
  }, [initialized, elemRef]);

  return (
    <div
      className={`sticky ${className || ""}`}
      ref={elemRef}
      id={id}
      data-sticky
      data-anchor={anchor}
      data-sticky-on={`large`}
      data-dynamic-height={`true`}
    >
      {children}
    </div>
  );
};

export default Sticked;
