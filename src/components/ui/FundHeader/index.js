import React from "react";
import Dates from "utils/dates";

import "./style.scss";

const FundHeader = ({ className }) => {
  return (
    <div
      className={`grid-x grid-padding-x grid-padding-y align-bottom fund-header ${
        className || ""
      }`}
    >
      <div className="cell small-12 medium-4">Fundo</div>
      <div className="cell small-3 medium-auto">
        <div className="text-right">Data da Cota</div>
      </div>
      <div className="cell small-3 medium-1">Mês(%)</div>
      <div className="cell small-3 medium-1">
        {Dates.fullYear(new Date())}(%)
      </div>
      <div className="cell small-3 medium-1">12M(%)</div>
      <div className="cell small-5 medium-auto">
        <div className="text-right">Aplicação mínima(R$)</div>
      </div>
      <div className="cell small-5 medium-1">
        <div className="text-right">Prazo do Resgate</div>
      </div>
      <div className="cell medium-1 shrink">Aplicar</div>
    </div>
  );
};

export default FundHeader;
