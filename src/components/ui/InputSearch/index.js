import React, { useState } from "react";
import "./style.scss";

const keys = {
  ignore: (key) =>
    [9, 17, 16, 18, 19, 20, 33, 34, 35, 36, 91, 93].includes(key),
  send: (key) => [9, 13].includes(key),
  clear: (key) => [27].includes(key),
};

const InputSearch = ({ onChange, placeholder }) => {
  const [value, setValue] = useState();

  const execute = () => {
    onChange({
      term: value,
    });
  };

  const handleKeyDown = (e) => {
    const key = e.keyCode || e.which;
    if (keys.send(key)) {
      execute();
    } else if (keys.clear(key)) {
      e.target.value = "";
      setValue("");
    }
  };

  const handleBlur = () => {
    execute();
  };

  const handleChange = (e) => {
    setValue(e.target.value);
  };

  return (
    <div className="input-container">
      <input
        type="text"
        className="input-search"
        placeholder={placeholder}
        onKeyDown={handleKeyDown}
        onChange={handleChange}
        onBlur={handleBlur}
      />
      <span className="mdi mdi-magnify icon" />
    </div>
  );
};

export default InputSearch;
