import React from "react";

const BgImage = ({ img, color, children, className }) => {
  return <div className={`bg-image ${className || ""}`}>{children}</div>;
};

export default BgImage;
