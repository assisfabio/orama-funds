import React from "react";
import "./style.scss";

const Hero = ({ children }) => {
  return (
    <div className="callout large">
      <div className="text-center">{children}</div>
    </div>
  );
};

export default Hero;
