import React, { useEffect, useRef, useState } from "react";
import { InputRisk, InputSearch, InputSlider, Skeletons } from "components/ui";
import Numbers from "utils/numbers";

const Search = ({ onChange, minimalRange, rescueRange, riskRange }) => {
  const [search, setSearch] = useState();
  const [minimal, setMinimal] = useState();
  const [risk, setRisk] = useState();
  const [rescue, setRescue] = useState();

  const elemRef = useRef();

  const getMinimal = (value) => {
    return minimalRange && minimalRange[value];
  };

  const getRescue = (value) => {
    return rescueRange && rescueRange[value];
  };

  const execute = () => {
    if (minimal && rescue && risk) {
      onChange({
        ...search,
        minimal: getMinimal(minimal),
        rescue: getRescue(rescue),
        risk: risk,
      });
    }
  };

  const handleChangeSearch = (term) => {
    setSearch(term);
  };

  const handleMinimalChange = (value) => {
    setMinimal(value);
  };

  const handleRiskChange = (value) => {
    setRisk(value);
  };

  const handleRescueChange = (value) => {
    setRescue(value);
  };

  useEffect(() => {
    if (elemRef?.current && search !== undefined) {
      execute();
    }
    // eslint-disable-next-line
  }, [search]);

  useEffect(() => {
    if (
      elemRef?.current &&
      minimalRange &&
      getMinimal(minimal) >= minimalRange[0]
    ) {
      execute();
    }
    // eslint-disable-next-line
  }, [minimal]);

  useEffect(() => {
    if (
      elemRef?.current &&
      rescueRange &&
      getRescue(rescue) >= rescueRange[0]
    ) {
      execute();
    }
    // eslint-disable-next-line
  }, [rescue]);

  useEffect(() => {
    if (elemRef?.current && riskRange && risk >= riskRange[0]) {
      execute();
    }
    // eslint-disable-next-line
  }, [risk]);

  return (
    <div className="card search" ref={elemRef}>
      <div className="card-section">
        <div className="grid-x grid-margin-x search-input">
          <div className="cell small-12 medium-7">
            <InputSearch
              placeholder="Buscar fundo por nome"
              onChange={handleChangeSearch}
            />
          </div>
        </div>
        <div className="grid-x grid-padding-x grid-padding-y controls">
          <div className="cell small-12 medium-4">
            {minimalRange ? (
              <>
                <label>Aplicação mínima</label>
                <InputSlider
                  id="minimalInp"
                  onChanged={handleMinimalChange}
                  start={0}
                  end={minimalRange?.length - 1}
                  initial={minimalRange?.length - 1}
                />
                <p className="controls-feedback">
                  Até {Numbers.format.currency(getMinimal(minimal))}
                </p>
              </>
            ) : (
              <>
                <Skeletons type="line" width="80%" height="15px" />
                <Skeletons type="line" height="20px" />
                <Skeletons type="line" width="60%" height="15px" />
              </>
            )}
          </div>
          <div className="cell small-12 medium-4">
            {riskRange ? (
              <>
                <label>Perfil de risco do fundo</label>
                <InputRisk
                  id="riskInp"
                  onChanged={handleRiskChange}
                  riskRange={riskRange}
                />
              </>
            ) : (
              <>
                <Skeletons type="line" width="80%" height="45px" />
                <Skeletons type="line" height="20px" />
                <Skeletons type="line" width="60%" height="20px" />
              </>
            )}
          </div>
          <div className="cell small-12 medium-4">
            {rescueRange ? (
              <>
                <label>Prazo de resgate</label>
                <InputSlider
                  id="rescueInp"
                  onChanged={handleRescueChange}
                  start={0}
                  end={rescueRange?.length - 1}
                  initial={rescueRange?.length - 1}
                />
                <p className="controls-feedback">
                  Até {getRescue(rescue)} dias úteis
                </p>
              </>
            ) : (
              <>
                <Skeletons type="line" width="80%" height="15px" />
                <Skeletons type="line" height="20px" />
                <Skeletons type="line" width="60%" height="15px" />
              </>
            )}
          </div>
        </div>
        <small className="text-medium-gray">
          Horário limite de aplicação: 12:00
        </small>
      </div>
    </div>
  );
};

export default Search;
