import React, { useEffect, useRef, useState } from "react";
import * as Foundation from "foundation-sites";
import $ from "jquery";

const Filters = ({ id, dataSource, onChange }) => {
  const elemRef = useRef();
  const [initialized, setInitialized] = useState(false);
  const [data, setData] = useState(dataSource);

  const [filters, setFilters] = useState([]);
  const [macros, setMacros] = useState([]);

  const handleSelectAll = (e) => {
    const selecteds = data
      .find((item) => `${item.id}` === `${e.target.value}`)
      .mains.map((main) => main.id);

    const partialMacros = [...macros];

    if (e.target.checked) {
      setFilters(filters.filter((item) => !selecteds.includes(item)));
      partialMacros.splice(
        partialMacros.findIndex((item) => {
          return item === e.target.value;
        }),
        1
      );
      setMacros(partialMacros);
    } else {
      setFilters(filters.concat(selecteds));
      setMacros([...macros, e.target.value]);
    }
  };

  const handleToggleFilters = (id) => {
    const partialFilters = [...filters];
    if (partialFilters.includes(id)) {
      partialFilters.splice(
        partialFilters.findIndex((item) => item === id),
        1
      );
    } else {
      partialFilters.push(id);
    }
    setFilters(partialFilters);
  };

  useEffect(() => {
    if (dataSource) {
      setData(dataSource);
    }
  }, [dataSource]);

  useEffect(() => {
    if (elemRef?.current && !initialized && data) {
      setInitialized(true);
    }
  }, [elemRef, initialized, data]);

  useEffect(() => {
    const elem = elemRef.current;
    if (initialized && data) {
      new Foundation.Accordion($(elem));
    }
  }, [initialized, data]);

  useEffect(() => {
    onChange(filters);
  }, [filters, onChange]);

  return (
    <div
      className="accordion filters"
      data-accordion
      data-allow-all-closed="true"
      ref={elemRef}
      id={id}
    >
      {data &&
        data.map((macro, i) => (
          <div className="accordion-item" data-accordion-item key={i}>
            <input
              type="checkbox"
              name={`select_all_${macro.id}`}
              value={macro.id}
              onChange={handleSelectAll}
              checked={!macros.includes(`${macro.id}`)}
            />
            <a href={`#${macro.id}`} className="accordion-title">
              {macro.name}
            </a>
            <div className="accordion-content" data-tab-content>
              {macro.mains.map((main, ii) => (
                <div className="filter-item" key={ii}>
                  <label>
                    <input
                      type="checkbox"
                      value={main.id}
                      name={`filter_group_${macro.id}`}
                      onChange={() => handleToggleFilters(main.id)}
                      checked={!filters.includes(main.id)}
                    />
                    {main.name}
                  </label>
                </div>
              ))}
            </div>
          </div>
        ))}
    </div>
  );
};

export default Filters;
