const language = "pt-BR";
const Dates = {
  options: {
    timeZone: "America/Sao_Paulo",
  },
  default: (date) =>
    new Intl.DateTimeFormat(language, { ...Dates.options }).format(
      date instanceof Date ? date : new Date(`${date}T00:00:00-03:00`)
    ),
  fullYear: (date) =>
    new Intl.DateTimeFormat(language, {
      ...Dates.options,
      year: "numeric",
    }).format(date instanceof Date ? date : new Date(`${date}T00:00:00-03:00`)),
};

export default Dates;
