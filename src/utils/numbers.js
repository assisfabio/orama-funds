const language = "pt-BR";
const Numbers = {
  format: {
    currency: (number) => {
      return new Intl.NumberFormat(language, {
        style: "currency",
        currency: "BRL",
      }).format(number);
    },
    float: (number, digits = 2) => {
      return new Intl.NumberFormat(language, {
        minimumFractionDigits: digits,
        maximumFractionDigits: digits,
      }).format(number);
    },
  },
};

export default Numbers;
