import axios from "axios";

const apiUrl = `${process.env.PUBLIC_URL}/api`;

const modeling = (data) => {
  const model = {};
  const rescueRange = [];
  const minimalRange = [];
  const riskRange = [];

  for (let i = 0; i < data.length; i++) {
    const spec = {
      macro: data[i].specification?.fund_macro_strategy,
      main: data[i].specification?.fund_main_strategy,
    };

    if (!model[spec.macro.name]) {
      model[spec.macro.name] = {
        ...spec.macro,
        mains: {},
      };
    }

    if (!model[spec.macro.name].mains[spec.main.name]) {
      model[spec.macro.name].mains[spec.main.name] = {
        ...spec.main,
        funds: [],
      };
    }

    model[spec.macro.name].mains[spec.main.name].funds.push(data[i]);

    if (
      !minimalRange.includes(
        data[i].operability.minimum_initial_application_amount
      )
    ) {
      minimalRange.push(data[i].operability.minimum_initial_application_amount);
    }

    if (!rescueRange.includes(data[i].operability.retrieval_quotation_days)) {
      rescueRange.push(data[i].operability.retrieval_quotation_days);
    }

    if (
      !riskRange.includes(
        data[i].specification.fund_risk_profile.score_range_order
      )
    ) {
      riskRange.push(data[i].specification.fund_risk_profile.score_range_order);
    }
  }

  const dataSource = [];
  Object.keys(model).map((item) => {
    const macro = {
      name: model[item].name,
      description: model[item].explanation,
      id: model[item].id,
      mains: [],
    };
    Object.keys(model[item].mains).map((main) =>
      macro.mains.push(model[item].mains[main])
    );
    return dataSource.push(macro);
  });

  /**
   * TODO Melhorar essa função com mais tempo e menos sono
   */
  return {
    data: dataSource,
    minimalRange: minimalRange.sort((a, b) => a - b),
    rescueRange: rescueRange.sort((a, b) => a - b),
    riskRange: riskRange.sort((a, b) => a - b),
  };
};

const filter = (data, excludes, search) => {
  const result = [];
  data &&
    data.forEach((macro) => {
      const mains = macro.mains.filter((main) => {
        if (!excludes.includes(main.id)) {
          const funds = main.funds.filter((fund) => {
            const hasName =
              !search.term ||
              fund.simple_name
                .toLowerCase()
                .indexOf(search.term.toLowerCase()) >= 0 ||
              fund.full_name.toLowerCase().indexOf(search.term.toLowerCase()) >=
                0 ||
              fund.slug.toLowerCase().indexOf(search.term.toLowerCase()) >= 0;
            const rescueDate =
              (!search.rescue && search.rescue !== 0) ||
              parseInt(fund.operability.retrieval_quotation_days) <=
                parseInt(search.rescue);

            const minimalAmount =
              (!search.minimal && search.minimal !== 0) ||
              parseFloat(fund.operability.minimum_initial_application_amount) <=
                parseFloat(search.minimal);

            const riskScore =
              (!search.risk && search.risk !== 0) ||
              parseInt(
                fund.specification.fund_risk_profile.score_range_order
              ) <= parseInt(search.risk);

            return hasName && rescueDate && minimalAmount && riskScore;
          });

          if (funds.length) {
            main.funds = funds;
            return true;
          }
          return false;
        }
        return false;
      });

      if (mains && mains.length) {
        result.push({
          ...macro,
          mains: mains,
        });
      }
    });
  return result;
};

const FundsServices = {
  getFunds: () => {
    return axios
      .get(`${apiUrl}/fund_detail_full.json`)
      .then((response) => modeling(response.data), [])
      .catch((error) => error);
  },
  filter: filter,
};

export default FundsServices;
